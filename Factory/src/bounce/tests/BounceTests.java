/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bounce.tests;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author Cameron
 */

public class BounceTests {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        EventQueue.invokeLater(() ->{
            JFrame frame = new BounceFrame();
            frame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);
        });
    }
}
    
    class BounceFrame extends JFrame{
        
        private BallComponent comp;
        public static final int STEPS = 1000;
        public static final int DELAY = 3;
        
        public BounceFrame(){
            setTitle("Bounce");
            comp = new BallComponent();
            JPanel buttonPanel = new JPanel();
            addButton(buttonPanel, "Start" , event -> addBall());
            addButton(buttonPanel, "Close" , event -> System.exit(0));
            add(buttonPanel, BorderLayout.SOUTH);
        }
        
        public void addButton(Container c, String title, ActionListener listener){
            JButton button = new JButton(title);
            c.add(button);
            button.addActionListener(listener);
        }
        
        public void addBall(){
            try{
                Ball ball = new Ball();
                comp.add(ball);

                
                for (int i = 1; i <= STEPS; i++){
                    ball.move(comp.getBounds());
                    comp.paint(comp.getGraphics());
                    Thread.sleep(DELAY);
                }
            }
            catch (InterruptedException e){
                
            }
        }
    }   


/*        public void addBall(){
                Ball ball = new Ball();
                comp.add(ball);
                Runnable r = () ->{
                    try{
                
                    for (int i = 1; i <= STEPS; i++){
                        ball.move(comp.getBounds());
                        comp.paint(comp.getGraphics());
                        Thread.sleep(DELAY);
                    }
                }
                catch (InterruptedException e){
                
                }
            };
            Thread t = new Thread(r);
            t.start();
        }
    }

*/

